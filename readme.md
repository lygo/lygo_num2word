# LyGo Num2Word

Convert numbers to words.

## How to Use

To use just call:

`go get -u bitbucket.org/lygo/lygo_num2word`

## Dependencies

NONE

### Versioning

Sources are versioned using git tags:

```
git tag v0.1.2
git push origin v0.1.2
```